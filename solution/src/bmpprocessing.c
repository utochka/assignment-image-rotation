#include "bmpprocessing.h"
#include "image.h"
#include <stdbool.h>
#include <stddef.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)


/*initializing an empty structure*/
static struct bmp_header new_empty_header(void){
    struct bmp_header new = {
            .bfType = 0x4D42,
            .bfReserved = 0,
            .biSize = 40,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
            .bfileSize = 0,
            .bOffBits = 0,
            .biWidth = 0,
            .biHeight = 0,
            .biSizeImage = 0
    };
    return new;
}

/*checking whether the header is read correctly*/
static bool check_reading_header(FILE* f, struct bmp_header* bmp){
    if ( fread( bmp, sizeof(struct bmp_header), 1, f) != 1 ) return 0;
    return 1;
}

/*calculation of the padding*/
static uint32_t pad(uint32_t w) {
    return (4 - (w * 3 % 4)%4);
}

/*checking whether the pixel array is read correctly*/
static int64_t check_reading_pixel_array(struct image* image, FILE* f){
    for (size_t i = 0; i < image->height; i=i+1) {
        for (size_t j = i*image->width; j<image->width*(i+1); j=j+1)
            if (fread( &(image->data[j]), sizeof(struct pixel), 1, f) != 1) {
                destroy_image(*image);
                return -1;
            }
        if (fseek(f, pad(image->width), SEEK_CUR) != 0) {
            destroy_image(*image);
            return -2;
        }
    }
    return 0;
}

/*checking the correctness of the conversion
  from an external format to an internal one*/
enum read_status from_bmp( FILE* in, struct image* img ) {

    struct bmp_header new_header = new_empty_header();
    if (check_reading_header(in, &new_header) == 0) return READ_INVALID_HEADER;

    *img = create_image(new_header.biWidth, new_header.biHeight);
    int64_t check = check_reading_pixel_array( img, in);
    if (check == -1) return READ_INVALID_BITS;
    if (check == -2) return READ_INVALID_PADDING;

    return READ_OK;
}

/*filling in changing fields in an empty structure*/
static struct bmp_header new_full_header(struct image const* rot_image) {
    struct bmp_header new = new_empty_header();
    new.bfileSize = (sizeof(struct bmp_header)) +
                         (sizeof(struct pixel) * rot_image->height * rot_image->width);
    new.bOffBits = sizeof(struct bmp_header);
    new.biWidth = rot_image->width;
    new.biHeight = rot_image->height;
    new.biSizeImage = sizeof(struct pixel) *
                           rot_image->width * rot_image->height;
    return new;
}

/*checking whether the header is written correctly*/
static bool check_writing_header(struct bmp_header* bmp, FILE* f){
    if ( fwrite( bmp, sizeof(struct bmp_header), 1, f) != 1 ) return 0;
    return 1;
}

/*checking whether the pixel array is written correctly*/
static int64_t check_writing_pixel_array(struct image* img, FILE* f){
    char padding[3] = {0};
    for (size_t i = 0; i < img->height; i=i+1) {
        for (size_t j =  i*img->width; j < img->width*(i+1); j=j+1)
            if (fwrite( &(img->data[j]) , sizeof(struct pixel), 1, f) != 1) return -1;
        if (fwrite(&padding, (int8_t)pad(img->width), 1, f) != 1) return -2;
    }
    return 0;
}

/*checking the correctness of the conversion
  from an internal format to an external one*/
enum write_status to_bmp( FILE* out, struct image* img ) {
    struct bmp_header new_header = new_full_header(img);
    if (check_writing_header( &new_header, out) == 0) return WRITE_INVALID_HEADER;

   // struct image new_pixel_array = create_image(new_header.biWidth, new_header.biHeight);
    int64_t check = check_writing_pixel_array( img, out);
    if ( check == -1) return WRITE_INVALID_BITS;
    if (check == -2) return WRITE_INVALID_PADDING;

    return WRITE_OK;
}
