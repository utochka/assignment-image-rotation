#include "imageprocessing.h"

static void swap_sizes( struct image const* old, struct image* new ) {
    new->width = old->height;
    new->height = old->width;
}

static struct pixel getter ( struct image const* image, uint64_t i, uint64_t j ){
    return image->data[( (image->width)*(image->height-j-1)) +i];
}

static void setter ( struct image const* image, struct pixel* new, uint64_t i, uint64_t j ){
    new[i*(image->height)+j] = getter(image, i, j);
}

//функция для поворота
static void transform_array (const struct image* image, struct pixel* new) {
    for (int64_t i = 0; i < image->width; ++i)
        for (int64_t j = 0; j < image->height; ++j)
            setter(image, new, i, j);
          //new[i*(image->height)+j] = image->data[( (image->width)*(image->height-j-1)) +i];
}

struct image rotate( struct image const source ) {
    struct image new = create_image(source.width, source.height );
    swap_sizes(&source, &new);
    transform_array(&source, new.data);
    return new;
}
