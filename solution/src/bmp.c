#include "bmp.h"

enum open file_open (FILE** file, const char* fname, enum mode m) {

    if (m == WRITE) {
        *file = fopen(fname, "wb");
        return OPEN_OK;
    }
    if (m == READ) {
        *file = fopen(fname, "rb");
        return OPEN_OK;
    }
    return OPEN_FAIL;
}


enum close file_close (FILE* file) {
    if (fclose(file) == 0) return CLOSE_OK;
    return CLOSE_FAIL;
}

