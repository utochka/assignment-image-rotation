#include "image.h"
#include <stdlib.h>

struct pixel* create_pixel_array(uint64_t w, uint64_t h) {
    return (struct pixel*)(malloc( sizeof(struct pixel) *w*h));
}

struct image create_image(uint64_t w, uint64_t h){
    return (struct image){.width = w, .height = h, .data = create_pixel_array(w, h)};
}

void destroy_image (struct image image) {
    free(image.data);
}
