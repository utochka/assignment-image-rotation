#include "bmpprocessing.h"
#include "imageprocessing.h"

#define OH_ERROR 1

int main( int argc, char** argv ) {

    if (argc != 3) return OH_ERROR;

    struct image source_img;
    FILE* in = NULL;
    FILE* out = NULL;

    if (file_open(&in,argv[1], READ) != OPEN_OK) return OH_ERROR;
    if (from_bmp(in, &source_img) != READ_OK) return OH_ERROR;
    if (file_close(in) != CLOSE_OK) return OH_ERROR;

    struct image rotated_image = rotate(source_img);

    if (file_open(&out,argv[2], WRITE) != OPEN_OK) return OH_ERROR;
    if (to_bmp(out, &rotated_image) != WRITE_OK) return OH_ERROR;
    if (file_close(out) != CLOSE_OK) return OH_ERROR;

    destroy_image(source_img);
    destroy_image(rotated_image);

    return 0;
}
