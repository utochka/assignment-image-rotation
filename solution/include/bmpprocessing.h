

#ifndef ASSIGNMENT_IMAGE_ROTATIONNN_FILEPROCESSING_H
#define ASSIGNMENT_IMAGE_ROTATIONNN_FILEPROCESSING_H

#include "bmp.h"
#include "image.h"

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PADDING
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_BITS,
    WRITE_INVALID_PADDING
};

enum write_status to_bmp( FILE* out, struct image* img );



#endif //ASSIGNMENT_IMAGE_ROTATIONNN_FILEPROCESSING_H
