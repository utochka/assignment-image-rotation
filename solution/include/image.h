

#ifndef ASSIGNMENT_IMAGE_ROTATIONNN_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATIONNN_IMAGE_H
#include <stdint.h>
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

struct image create_image(uint64_t w, uint64_t h);
struct pixel* create_pixel_array(uint64_t w, uint64_t h);
void destroy_image (struct image image);


#endif //ASSIGNMENT_IMAGE_ROTATIONNN_IMAGE_H
