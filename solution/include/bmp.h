
#ifndef ASSIGNMENT_IMAGE_ROTATIONNN_BMP_H
#define ASSIGNMENT_IMAGE_ROTATIONNN_BMP_H

#include  <stdint.h>
#include <stdio.h>


enum open {
    OPEN_OK = 0,
    OPEN_FAIL
};

enum close {
    CLOSE_OK = 0,
    CLOSE_FAIL
};

enum mode {
    READ,
    WRITE
};

enum open file_open (FILE** file, const char* fname, enum mode m);
enum close file_close (FILE* file);


#endif //ASSIGNMENT_IMAGE_ROTATIONNN_BMP_H
